import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../domain/kelas_set.dart';
import '../../../services/graphql_service.dart';

import '../../../services/graphql_queries.dart';

// TODO refactor to family provider
final getKelasWaliPegawai = FutureProvider<KelasSet?>((ref) async {
  final user = ref.watch(getMeInfoProvider).value;
  if (user?.pegawai == null) {
    return null;
  }
  final pegawai = user!.pegawai!;
  final gqlClient = await ref.watch(graphQLClientProvider.future);
  final options = QueryOptions(
      document: gql(Queries.waliKelasPegawaiByID),
      variables: <String, String>{
        'id': pegawai.id,
      });
  final result = await gqlClient.query(options);
  final kelasSet = KelasSet.fromJson(result.data!['pegawai']['kelasSet']);
  return kelasSet;
});
