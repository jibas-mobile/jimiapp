import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../login/presentation/login_view_model.dart';

class TabItemAccount extends ConsumerWidget {
  const TabItemAccount({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final authState = ref.watch(authStateProvider.notifier);
    return Column(
      children: [
        Container(
          width: double.infinity,
          padding: const EdgeInsets.all(16.0),
          child: SafeArea(
            child: Column(
              children: [
                Text('Account'),
                const SizedBox(
                  height: 16.0,
                ),
                TextButton(onPressed: authState.logout, child: Text('Logout'))
              ],
            ),
          ),
        ),
      ],
    );
  }
}
