import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../common_widgets/separasi_baris.dart';
import 'home_search_box.dart';
import 'user_info_box.dart';

class TabItemHome extends ConsumerWidget {
  const TabItemHome({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          color: Theme.of(context).appBarTheme.backgroundColor,
          padding: const EdgeInsets.all(16.0),
          child: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: const [
                HomeSearchBox(),
                SizedBox(height: 16.0),
                UserInfoBox(),
                SeparasiBaris(),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
