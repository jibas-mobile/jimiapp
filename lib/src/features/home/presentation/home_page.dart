import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'tab_item_account.dart';
import 'tab_item_berita.dart';
import 'organisasi_headline.dart';
import 'tab_item_home.dart';


class HomePage extends ConsumerStatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  StatefulHomePage createState() => StatefulHomePage();
}

class StatefulHomePage extends ConsumerState {
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  static const List<Widget> _tabContents = <Widget>[
    TabItemHome(),
    TabItemBerita(),
    TabItemAccount(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const OrganisasiHeadline(),
        backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
      ),
      body: _tabContents.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.home_rounded), label: tr('home.tabs.home')),
          BottomNavigationBarItem(
              icon: Icon(Icons.newspaper), label: tr('home.tabs.news')),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_circle_rounded), label: tr('home.tabs.account')),
        ],
      ),
    );
  }
}


