import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:jimiapp/src/constants/values.dart';
import 'package:jimiapp/src/features/home/application/get_wali_kelas_pegawai.dart';

import '../../../services/graphql_service.dart';

class UserInfoBox extends ConsumerWidget {
  const UserInfoBox({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final userInfo = ref.watch(getMeInfoProvider);
    final kelasWaliPegawai = ref.watch(getKelasWaliPegawai);

    // handle offline
    // TODO refactor this
    ref.watch(getMeInfoProvider).whenOrNull(error: (error, _) async {
      String errorMsg = '';
      if (error is ServerException) {
        errorMsg = 'tidak dapet terkoneksi ke server';
      } else {
        errorMsg = 'terdapat masalah: ' + error.toString();
      }
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(errorMsg),
          action: SnackBarAction(
            label: 'Retry',
            onPressed: () {
              ref.refresh(getMeInfoProvider);
              ref.refresh(getKelasWaliPegawai);
            },
          ),
          duration: ActionDurations.wholeYearDuration,
        ),
      );
    });

    return Container(
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              color: Colors.white,
            ),
            borderRadius: const BorderRadius.all(Radius.circular(10))),
        child: userInfo.when(
          data: (user) {
            final nama = user.pegawai?.nama ?? '';
            final List<Widget> rows = [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Text(
                      'Hi $nama',
                      style: Theme.of(context).textTheme.titleSmall,
                    ),
                    const SizedBox(width: 8.0),
                    Chip(
                      padding: EdgeInsets.all(0),
                      backgroundColor: Theme.of(context).primaryColorLight,
                      label: Text(user.pegawai?.bagian?.nama ?? '',
                          style: Theme.of(context).textTheme.labelSmall),
                    ),
                  ],
                ),
              )
            ];

            if (user.pegawai?.guru != null) {
              final pelajaran = user.pegawai!.guru!.pelajaran;
              rows.add(const Divider());
              rows.add(Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                        'Anda adalah guru ${pelajaran.nama} di ${pelajaran.departemen.nama}'),
                    const Icon(Icons.chevron_right),
                  ],
                ),
              ));
            }

            try {
              final kelasSet = kelasWaliPegawai.value;
              if (kelasSet != null) {
                rows.add(const Divider());
                rows.add(Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                          'Anda adalah wali dari ${kelasSet.edges.length} kelas'),
                      const Icon(Icons.chevron_right),
                    ],
                  ),
                ));
              }
            } catch (e) {}

            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: rows,
            );
          },
          error: (e, __) => Text('unable to load: $e'),
          loading: () => const CircularProgressIndicator(),
        ));
  }
}
