import 'package:flutter/material.dart';

import '../../../routing/app_router.dart';

class HomeSearchBox extends StatelessWidget {
  const HomeSearchBox({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(
            color: Colors.white,
          ),
          borderRadius: const BorderRadius.all(Radius.circular(10))
      ),
      padding: EdgeInsets.all(5),
      child: InkWell(
        onTap: () async {
          await Navigator.of(context).pushNamed(
            AppRoutes.searchPage,
          );
        },
        child: Hero(
          tag: 'searchField',
          child: Material(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Text('Cari Siswa, Pegawai, Kelas',
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.left,
                      style: Theme.of(context).textTheme.bodySmall!.copyWith(fontFamily: 'Montserrat',),
                  ),
                ),
                const Icon(Icons.search, color: Colors.grey,)
              ],
            ),
          ),
        ),
      ),
      // color: Colors.white,
    );
  }
}
