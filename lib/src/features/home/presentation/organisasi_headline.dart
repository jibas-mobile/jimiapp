import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../services/organisasi_service.dart';

class OrganisasiHeadline extends ConsumerWidget {
  const OrganisasiHeadline({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final orgName = ref.watch(organisasiServiceProvider.notifier).getOrganisasi?.nama ?? '';
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            child: Text(orgName,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.left,
                style: Theme.of(context).primaryTextTheme.titleMedium!.copyWith(fontFamily: 'Montserrat',)),
          ),
          const Icon(Icons.notifications, color: Colors.white,)
        ],
      ),
    );
  }
}