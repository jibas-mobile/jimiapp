import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:jimiapp/src/features/akademik/domain/kelas.dart';

part 'gql_conn_node_kelas.freezed.dart';
part 'gql_conn_node_kelas.g.dart';

@freezed
class GqlConnNodeKelas with _$GqlConnNodeKelas {
  factory GqlConnNodeKelas({
    required Kelas kelas,
  }) = _GqlConnNode;

  factory GqlConnNodeKelas.fromJson(Map<String, dynamic> json) => _$GqlConnNodeKelasFromJson(json);
}
