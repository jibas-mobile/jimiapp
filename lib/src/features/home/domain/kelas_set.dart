import 'package:freezed_annotation/freezed_annotation.dart';
import 'gql_conn_edges.dart';
import 'gql_conn_page_info.dart';

part 'kelas_set.freezed.dart';
part 'kelas_set.g.dart';

@freezed
class KelasSet with _$KelasSet {
  factory KelasSet({
    required GqlConnPageInfo pageInfo,
    required List<GqlConnEdgesKelasSet> edges,
  }) = _KelasSet;

  factory KelasSet.fromJson(Map<String, dynamic> json) => _$KelasSetFromJson(json);
}
