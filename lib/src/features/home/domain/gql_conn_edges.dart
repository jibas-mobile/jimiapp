import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:jimiapp/src/features/akademik/domain/kelas.dart';

import 'gql_conn_node_kelas.dart';

part 'gql_conn_edges.freezed.dart';
part 'gql_conn_edges.g.dart';

@freezed
class GqlConnEdgesKelasSet with _$GqlConnEdgesKelasSet {
  factory GqlConnEdgesKelasSet({
    required Kelas node,
  }) = _GqlConnEdgesKelasSet;

  factory GqlConnEdgesKelasSet.fromJson(Map<String, dynamic> json) => _$GqlConnEdgesKelasSetFromJson(json);
}
