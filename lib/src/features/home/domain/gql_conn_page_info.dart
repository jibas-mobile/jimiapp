import 'package:freezed_annotation/freezed_annotation.dart';

part 'gql_conn_page_info.freezed.dart';
part 'gql_conn_page_info.g.dart';

@freezed
class GqlConnPageInfo with _$GqlConnPageInfo {
  factory GqlConnPageInfo({
    bool? hasNextPage,
    bool? hasPreviousPage,
    String? startCursor,
    String? endCursor,
  }) = _GqlConnPageInfo;

  factory GqlConnPageInfo.fromJson(Map<String, dynamic> json) => _$GqlConnPageInfoFromJson(json);
}
