import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../../../utils/top_providers.dart';
import '../../akademik/domain/pegawai.dart';
import '../application/search_service.dart';

class SearchTabPegawai extends ConsumerStatefulWidget {
  const SearchTabPegawai({Key? key}) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      SearchTabPegawaiState();
}

class SearchTabPegawaiState extends ConsumerState<SearchTabPegawai> {
  final PagingController<int, Pegawai> _pagingController =
      PagingController(firstPageKey: 0);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
  }

  Future<void> _fetchPage(int pageKey) async {
    final searchService = await ref.watch(searchServiceProvider.future);
    final nama = ref.watch(searchKeywordProvider);
    try {
      final result = await searchService.searchPegawai(
          offset: pageKey, len: 10, nama: nama);
      final List<Pegawai> staffs = [];
      for (var element in result.edges) {
        staffs.add(element.node);
      }
      if (result.pageInfo.hasNextPage!) {
        final nextPageKey = pageKey + result.edges.length;
        _pagingController.appendPage(staffs, nextPageKey);
      } else {
        _pagingController.appendLastPage(staffs);
      }
    } catch (e) {
      _pagingController.error = e;
    }
  }

  @override
  Widget build(BuildContext context) {
    ref.listen<String>(searchKeywordProvider, (_, nama) {
      _pagingController.refresh();
    });
    ref.watch(searchKeywordProvider);
    final logger = ref.watch(loggerProvider);
    logger.d('Rebuild search_tab_siswa');
    return PagedListView<int, Pegawai>(
      pagingController: _pagingController,
      builderDelegate: PagedChildBuilderDelegate<Pegawai>(
        itemBuilder: (context, item, index) => ListTilePegawai(pegawai: item),
      ),
    );
  }

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }
}

class ListTilePegawai extends StatelessWidget {
  const ListTilePegawai({
    required this.pegawai,
    Key? key,
  }) : super(key: key);

  final Pegawai pegawai;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      // TODO refactor this to new widget
      title: Text(pegawai.nama),
      subtitle: Text(pegawai.bagian.nama +
          ' - ' +
          (pegawai.guru?.pelajaran.nama ?? '')),
    );
  }
}
