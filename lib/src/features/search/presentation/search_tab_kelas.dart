import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../../../routing/app_router.dart';
import '../../../utils/top_providers.dart';
import '../../akademik/domain/kelas.dart';
import '../application/search_service.dart';

class SearchTabKelas extends ConsumerStatefulWidget {
  const SearchTabKelas({Key? key}) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => SearchTabKelasState();
}

class SearchTabKelasState extends ConsumerState<SearchTabKelas> {
  final PagingController<int, Kelas> _pagingController =
      PagingController(firstPageKey: 0);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
  }

  Future<void> _fetchPage(int pageKey) async {
    final searchService = await ref.watch(searchServiceProvider.future);
    final nama = ref.watch(searchKeywordProvider);
    try {
      final result =
          await searchService.searchKelas(offset: pageKey, len: 10, nama: nama);
      final List<Kelas> staffs = [];
      for (var element in result.edges) {
        staffs.add(element.node);
      }
      if (result.pageInfo.hasNextPage!) {
        final nextPageKey = pageKey + result.edges.length;
        _pagingController.appendPage(staffs, nextPageKey);
      } else {
        _pagingController.appendLastPage(staffs);
      }
    } catch (e) {
      _pagingController.error = e;
    }
  }

  @override
  Widget build(BuildContext context) {
    ref.listen<String>(searchKeywordProvider, (_, nama) {
      _pagingController.refresh();
    });
    ref.watch(searchKeywordProvider);
    final logger = ref.watch(loggerProvider);
    logger.d('Rebuild search_tab_siswa');
    return PagedListView<int, Kelas>(
      pagingController: _pagingController,
      builderDelegate: PagedChildBuilderDelegate<Kelas>(
        itemBuilder: (context, item, index) => ListTileKelas(kelas: item),
      ),
    );
  }
}

class ListTileKelas extends StatelessWidget {
  const ListTileKelas({
    required this.kelas,
    Key? key,
  }) : super(key: key);

  final Kelas kelas;

  @override
  Widget build(BuildContext context) {
    final tahunAjaran = kelas.tahunAjaran!;
    return GestureDetector(
      onTap: () async {
        await Navigator.of(context)
            .pushNamed(AppRoutes.viewKelasPage, arguments: kelas);
      },
      child: ListTile(
        // TODO refactor this to new widget
        title: Text('${kelas.tingkat.nama} ${kelas.nama}'),
        subtitle: Text('${tahunAjaran.departemen?.nama} ${tahunAjaran.nama}'),
      ),
    );
  }
}
