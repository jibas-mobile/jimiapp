
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../../akademik/domain/siswa.dart';
import '../application/search_service.dart';
import '../../../utils/top_providers.dart';
import 'list_tile_siswa.dart';

class SearchTabSiswa extends ConsumerStatefulWidget {
  const SearchTabSiswa({Key? key}) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => SearchTabSiswaState();
}

class SearchTabSiswaState extends ConsumerState<SearchTabSiswa> {

  final PagingController<int, Siswa> _pagingController =
  PagingController(firstPageKey: 0);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
  }

  Future<void> _fetchPage(int pageKey) async {
    final searchService = await ref.watch(searchServiceProvider.future);
    final nama = ref.watch(searchKeywordProvider);
    try {
      final result = await searchService.searchSiswa(offset: pageKey, len: 10, nama: nama);
      final List<Siswa> students = [];
      for (var element in result.edges) {
        students.add(element.node);
      }
      if (result.pageInfo.hasNextPage!) {
        final nextPageKey = pageKey + result.edges.length;
        _pagingController.appendPage(students, nextPageKey);
      } else {
        _pagingController.appendLastPage(students);
      }
    } catch (e) {
      _pagingController.error = e;
    }
  }

  @override
  Widget build(BuildContext context) {
    ref.listen<String>(searchKeywordProvider, (_, nama) {
      _pagingController.refresh();
    });
    ref.watch(searchKeywordProvider);
    final logger = ref.watch(loggerProvider);
    logger.d('Rebuild search_tab_siswa');
    return PagedListView<int, Siswa>(
      pagingController: _pagingController,
      builderDelegate: PagedChildBuilderDelegate<Siswa>(
        itemBuilder: (context, item, index) => ListTileSiswa(siswa: item),
      ),
    );
  }

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }
}

