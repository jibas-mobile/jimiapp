import 'package:flutter/material.dart';
import 'package:jimiapp/src/routing/app_router.dart';

import '../../akademik/domain/siswa.dart';

class ListTileSiswa extends StatelessWidget {
  const ListTileSiswa({
    required this.siswa,
    Key? key,
  }) : super(key: key);

  final Siswa siswa;

  static String createHeroTag(String id) {
    return 'namaSiswa-$id';
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        await Navigator.of(context).pushNamed(AppRoutes.viewSiswaPage, arguments: siswa);
      },
      child: ListTile(
          title: Hero(
              tag: createHeroTag(siswa.id),
              child: Text(siswa.nama)
          ),
          subtitle: Text((siswa.kelas.nama) + ' - ' + (siswa.kelas.tingkat.nama )),
      ),
    );
  }
}