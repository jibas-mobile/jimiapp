import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:jimiapp/src/features/search/application/search_service.dart';
import 'package:jimiapp/src/features/search/presentation/search_tab_kelas.dart';
import 'package:jimiapp/src/features/search/presentation/search_tab_pegawai.dart';
import 'package:jimiapp/src/features/search/presentation/search_tab_siswa.dart';
import 'package:jimiapp/src/utils/top_providers.dart';

class SearchForm extends ConsumerStatefulWidget {
  const SearchForm({Key? key}) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => SearchFormState();
}

class SearchFormState extends ConsumerState {
  bool _isTyping = false;
  late String _searchText;
  final _textEditingController = TextEditingController();
  late int _initialIndex;
  // final _tabController = DefaultTabController(length: 3, child: child);

  @override
  void initState() {
    _textEditingController.text = ref.read(searchKeywordProvider.notifier).state;
    // _initialIndex = ref.read()
  }

  @override
  Widget build(BuildContext context) {
    ref.watch(searchKeywordProvider.notifier);
    final logger = ref.read(loggerProvider);
    return Scaffold(
      body: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              onPressed: () => Navigator.pop(context),
              icon: const Icon(Icons.arrow_back),
            ),
            bottom: TabBar(
              onTap: (tabIndex) async {
                ref.watch(searchTypeProvider.notifier).state =
                    SearchType.values[tabIndex];
              },
              tabs: const [
                Tab(
                  icon: FaIcon(FontAwesomeIcons.person),
                  text: 'Siswa',
                ),
                Tab(
                    icon: FaIcon(FontAwesomeIcons.chalkboardUser),
                    text: 'Pegawai'),
                Tab(icon: FaIcon(FontAwesomeIcons.peopleGroup), text: 'Kelas'),
              ],
            ),
            title: Hero(
              tag: 'searchField',
              child: Material(
                color: Colors.transparent,
                child: TextField(
                    controller: _textEditingController,
                    textInputAction: TextInputAction.search,
                    onChanged: (search) async {
                      _searchText = search;
                      logger.d('Search input changed=[$search]');
                      if (_isTyping) {
                        return;
                      }
                      _isTyping = true;
                      await Future.delayed(const Duration(seconds: 1));
                      ref.watch(searchKeywordProvider.notifier).state =
                          _searchText;
                      _isTyping = false;
                    },
                    cursorColor: Colors.white,
                    style: const TextStyle(
                      color: Colors.white,
                    ),
                    autofocus: true,
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                        onPressed: () => _textEditingController.clear(),
                        icon: const Icon(Icons.clear),
                        color: Colors.white70,
                      ),
                      labelText: 'Cari Siswa, Pegawai, Kelas',
                      border: InputBorder.none,
                      fillColor: Colors.transparent,
                      labelStyle: const TextStyle(
                        color: Colors.white70,
                      ),
                      isDense: true,
                    )),
              ),
            ),
          ),
          body: const TabBarView(
            children: [
              SearchTabSiswa(),
              SearchTabPegawai(),
              SearchTabKelas(),
            ],
          ),
        ),
      ),
    );
  }
}
