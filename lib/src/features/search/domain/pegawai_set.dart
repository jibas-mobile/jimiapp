import 'package:freezed_annotation/freezed_annotation.dart';

import '../../home/domain/gql_conn_page_info.dart';
import 'pegawai_set_edge.dart';

part 'pegawai_set.freezed.dart';
part 'pegawai_set.g.dart';

@freezed
class PegawaiSet with _$PegawaiSet {
  factory PegawaiSet({
    required GqlConnPageInfo pageInfo,
    required List<PegawaiSetEdge> edges,
    required int totalCount,
  }) = _PegawaiSet;

  factory PegawaiSet.fromJson(Map<String, dynamic> json) => _$PegawaiSetFromJson(json);
}
