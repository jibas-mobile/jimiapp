import 'package:freezed_annotation/freezed_annotation.dart';

import '../../home/domain/gql_conn_page_info.dart';
import 'kelas_set_edge.dart';

part 'kelas_set.freezed.dart';
part 'kelas_set.g.dart';

@freezed
class KelasSet with _$KelasSet {
  factory KelasSet({
    required GqlConnPageInfo pageInfo,
    required List<KelasSetEdge> edges,
    required int totalCount,
  }) = _KelasSet;

  factory KelasSet.fromJson(Map<String, dynamic> json) =>
      _$KelasSetFromJson(json);
}
