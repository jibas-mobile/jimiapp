import 'package:freezed_annotation/freezed_annotation.dart';

import '../../akademik/domain/pegawai.dart';

part 'pegawai_set_edge.freezed.dart';
part 'pegawai_set_edge.g.dart';

@freezed
class PegawaiSetEdge with _$PegawaiSetEdge {
  factory PegawaiSetEdge({
    required Pegawai node,
  }) = _PegawaiSetEdge;

  factory PegawaiSetEdge.fromJson(Map<String, dynamic> json) => _$PegawaiSetEdgeFromJson(json);
}
