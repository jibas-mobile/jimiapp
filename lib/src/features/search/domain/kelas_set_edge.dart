import 'package:freezed_annotation/freezed_annotation.dart';

import '../../akademik/domain/kelas.dart';

part 'kelas_set_edge.freezed.dart';
part 'kelas_set_edge.g.dart';

@freezed
class KelasSetEdge with _$KelasSetEdge {
  factory KelasSetEdge({
    required Kelas node,
  }) = _KelasSetEdge;

  factory KelasSetEdge.fromJson(Map<String, dynamic> json) =>
      _$KelasSetEdgeFromJson(json);
}
