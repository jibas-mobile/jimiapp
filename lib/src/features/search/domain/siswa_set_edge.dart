import 'package:freezed_annotation/freezed_annotation.dart';
import '../../akademik/domain/siswa.dart';

part 'siswa_set_edge.freezed.dart';
part 'siswa_set_edge.g.dart';

@freezed
class SiswaSetEdge with _$SiswaSetEdge {
  factory SiswaSetEdge({
    required Siswa node,
  }) = _SiswaSetEdge;

  factory SiswaSetEdge.fromJson(Map<String, dynamic> json) => _$SiswaSetEdgeFromJson(json);
}
