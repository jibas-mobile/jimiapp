import 'package:freezed_annotation/freezed_annotation.dart';
import 'siswa_set_edge.dart';
import '../../home/domain/gql_conn_page_info.dart';

part 'siswa_set.freezed.dart';
part 'siswa_set.g.dart';

@freezed
class SiswaSet with _$SiswaSet {
  factory SiswaSet({
    required GqlConnPageInfo pageInfo,
    required List<SiswaSetEdge> edges,
    required int totalCount,
  }) = _SiswaSet;

  factory SiswaSet.fromJson(Map<String, dynamic> json) => _$SiswaSetFromJson(json);
}
