import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:logger/logger.dart';

import '../../../services/graphql_service.dart';
import '../../../utils/top_providers.dart';
import '../../home/domain/kelas_set.dart';
import '../domain/pegawai_set.dart';
import '../domain/siswa_set.dart';
import 'graphql_queries.dart';

enum SearchType { Siswa, Pegawai, Kelas }

final searchTypeProvider = StateProvider<SearchType>((_) => SearchType.Siswa);

final searchKeywordProvider = StateProvider<String>((_) => '');

final itemsPerPageProvider = Provider<int>((_) => 10);

class SearchSiswaArgs {
  SearchSiswaArgs({required this.offset, required this.len, this.nama});

  String? nama;
  int offset;
  int len;
}

final searchSiswaProvider =
    FutureProvider.family<SiswaSet, SearchSiswaArgs>((ref, info) async {
  final searchService = await ref.watch(searchServiceProvider.future);
  // ref.keepAlive();
  return await searchService.searchSiswa(
      offset: info.offset, len: info.len, nama: info.nama);
});

final searchServiceProvider = FutureProvider<SearchService>((ref) async {
  return SearchService(
      client: await ref.watch(graphQLClientProvider.future),
      // itemsPerPage: ref.watch(itemsPerPageProvider),
      logger: ref.watch(loggerProvider));
});

class SearchService {
  SearchService({
    required this.client,
    required this.logger,
  });

  final GraphQLClient client;
  final Logger logger;

  Future<SiswaSet> searchSiswa({
    required int offset,
    required int len,
    String? nama, // TODO add more filters
  }) async {
    logger.d('searchSiswa with $offset $len and $nama');
    // final offset = page * itemsPerPage;
    final options = QueryOptions(
        document: gql(Queries.searchSiswa),
        variables: <String, dynamic>{
          'first': len,
          'offset': offset,
          'nama': nama,
        });
    final result = await client.query(options);
    final siswaSet = SiswaSet.fromJson(result.data!['siswaSet']);
    return siswaSet;
  }

  Future<PegawaiSet> searchPegawai({
    required int offset,
    required int len,
    nama = '', // TODO add more filters
  }) async {
    logger.d('searchPegawai with $offset $len and $nama');
    final options = QueryOptions(
        document: gql(Queries.searchPegawai),
        variables: <String, dynamic>{
          'first': len,
          'offset': offset,
          'nama': nama,
        });
    final result = await client.query(options);
    final pegawaiSet = PegawaiSet.fromJson(result.data!['pegawaiSet']);
    return pegawaiSet;
  }

  Future<KelasSet> searchKelas({
    required int offset,
    required int len,
    nama = '', // TODO add more filters
  }) async {
    logger.d('searchhKelas with $offset $len and $nama');
    final options = QueryOptions(
        document: gql(Queries.searchKelas),
        variables: <String, dynamic>{
          'first': len,
          'offset': offset,
          'nama': nama,
        });
    final result = await client.query(options);
    final kelasSet = KelasSet.fromJson(result.data!['kelasSet']);
    return kelasSet;
  }
}
