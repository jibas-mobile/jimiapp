abstract class Queries {
  static const String searchSiswa = r'''
    query 
      findAllSiswa($first: Int!, $offset: Int!, $nama: String) {
        siswaSet(first: $first, offset: $offset, nama: $nama, orderBy: "nama") {
          totalCount
          edges {
            node {
              id
              nama
              kelas {
                id
                nama
                tingkat {
                  id
                  nama
                  departemen {
                    id
                    nama
                   }
                }
              }
            }
          }
          pageInfo {
            startCursor
            endCursor
            hasPreviousPage
            hasNextPage
          }
        }
      }
  ''';

  static const String searchPegawai = r'''
query findAllPegawai($first: Int!, $offset: Int!, $nama: String) {
	pegawaiSet(first: $first, offset: $offset, nama: $nama, orderBy: "nama") {
		totalCount
		pageInfo {
      startCursor
      endCursor
      hasPreviousPage
      hasNextPage
    }
		edges {
			node {
				id
				nip
				nama
				guru {
				  id
					pelajaran {
						id
						nama
						departemen {
							id
							nama
						}
					}
					status {
						id
						status
					}
				}
				bagian {
					id
					nama
				}
				
			}
		}
	}
}
  ''';

  static const String searchKelas = r'''
    
query {
	kelasSet {
		totalCount
		pageInfo {
				startCursor
				endCursor
				hasPreviousPage
				hasNextPage
			}
		edges {
			node {
				id
				nama
				wali {
					id
					nama
					nip
					bagian {
					  id
					  nama
					}
				}
				tingkat {
					id
					nama
					departemen {
						id
						nama
					}
				}
				tahunAjaran {
					id
					nama
					aktif
					tanggalMulai
					tanggalAkhir
					departemen {
						id
						nama
					}
				}
			}
		}
	}
}
''';
}
