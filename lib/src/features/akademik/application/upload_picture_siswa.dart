import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_picker/image_picker.dart';
import 'package:jimiapp/src/features/akademik/data/akademik_repository.dart';

class UploadProgress {
  const UploadProgress({
    required this.progress,
    required this.done,
  });

  final int progress;
  final bool done;
}

class UploadSiswaInfo {
  const UploadSiswaInfo({required this.id, required this.xFile});

  final String id;
  final XFile xFile;
}

final uploadPictureSiswaProvider = StateNotifierProvider.autoDispose
    .family<UploadPictureSiswa, UploadProgress, UploadSiswaInfo>((ref, info) {
  final akademikRepository = ref.watch(akademiRepositoryProvider).value!;
  ref.maintainState = true;
  return UploadPictureSiswa(
      uploadInfo: info, akademikRepository: akademikRepository);
});

class UploadPictureSiswa extends StateNotifier<UploadProgress> {
  UploadPictureSiswa({
    required this.uploadInfo,
    required this.akademikRepository,
  }) : super(const UploadProgress(progress: 0, done: false));

  final UploadSiswaInfo uploadInfo;
  final AkademikRepository akademikRepository;
  late int size;
  int sofar = 0;

  Future<void> startUpload() async {
    size = await uploadInfo.xFile.length();
    final read = uploadInfo.xFile.openRead();
    final mimeType = uploadInfo.xFile.mimeType;
    final response = await akademikRepository
        .uploadPhotoSiswa(uploadInfo.id, read, mimeType: mimeType, size: size,
            onSendProgress: (int count, int total) {
      sofar += count;
      final progress = ((sofar / size) * 100).round();
      state = UploadProgress(progress: progress, done: false);
    });
    state = const UploadProgress(progress: 100, done: true);
  }
}
