import 'package:freezed_annotation/freezed_annotation.dart';

import 'departemen.dart';

part 'pelajaran.freezed.dart';
part 'pelajaran.g.dart';

@freezed
class Pelajaran with _$Pelajaran {
  factory Pelajaran({
    required String id,
    required String nama,
    required Departemen departemen,
  }) = _Pelajaran;

  factory Pelajaran.fromJson(Map<String, dynamic> json) => _$PelajaranFromJson(json);
}
