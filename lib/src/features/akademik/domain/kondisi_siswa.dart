import 'package:freezed_annotation/freezed_annotation.dart';

part 'kondisi_siswa.freezed.dart';

part 'kondisi_siswa.g.dart';

@freezed
class KondisiSiswa with _$KondisiSiswa {
  factory KondisiSiswa({
    required String id,
    required String nama,
    String? info1,
    String? info2,
    String? info3,
    DateTime? createdAt,
    int? urutan,
  }) = _KondisiSiswa;

  factory KondisiSiswa.fromJson(Map<String, dynamic> json) => _$KondisiSiswaFromJson(json);
}
