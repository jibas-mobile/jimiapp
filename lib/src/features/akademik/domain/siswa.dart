import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:jimiapp/src/features/akademik/domain/kelas.dart';

part 'siswa.freezed.dart';

part 'siswa.g.dart';

@freezed
class Siswa with _$Siswa {
  factory Siswa({
    required String id,
    required String nama,
    required Kelas kelas,
    String? nis,
    String? nisn,
  }) = _Siswa;

  factory Siswa.fromJson(Map<String, dynamic> json) => _$SiswaFromJson(json);
}
