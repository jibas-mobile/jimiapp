import 'package:freezed_annotation/freezed_annotation.dart';

part 'agama.freezed.dart';

part 'agama.g.dart';

@freezed
class Agama with _$Agama {
  factory Agama({
    required String id,
    required String nama,
    String? info1,
    String? info2,
    String? info3,
    DateTime? createdAt,
    int? urutan,
  }) = _Agama;

  factory Agama.fromJson(Map<String, dynamic> json) => _$AgamaFromJson(json);
}
