import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:jimiapp/src/features/akademik/domain/kelas.dart';

import 'agama.dart';
import 'angkatan.dart';
import 'asal_sekolah.dart';
import 'jenis_pekerjaan.dart';
import 'kondisi_siswa.dart';
import 'status_siswa.dart';
import 'suku.dart';
import 'tingkat_pendidikan.dart';

part 'siswa_detail.freezed.dart';
part 'siswa_detail.g.dart';

enum JenisKelamin {
  @JsonValue("LAKILAKI")
  lakilaki,
  @JsonValue("PEREMPUAN")
  perempuan
}

extension JenisKelaminEnglish on JenisKelamin {
  String toEnglishValue() {
    switch (index) {
      case 0:
        return "male";
      case 1:
        return "female";
    }
    return "unspecified";
  }
}

@freezed
class SiswaDetail with _$SiswaDetail {
  const SiswaDetail._();
  factory SiswaDetail({
    required String id,
    required String nama,
    required Kelas kelas,
    required String nis,
    String? nisn,
    String? nik,
    String? noun,
    String? panggilan,
    required bool aktif,
    required int tahunMasuk,
    required Angkatan angkatan,
    Suku? suku,
    Agama? agama,
    String? fotoUrl,
    JenisKelamin? kelamin,
    String? tempatLahir,
    DateTime? tanggalLahir,
    String? warga,
    int? anakKe,
    int? jumlahSaudara,
    String? statusAnak,
    int? jumlahSaudaraKandung,
    int? jumlahSaudaraTiri,
    KondisiSiswa? kondisiSiswa,
    StatusSiswa? status,
    String? bahasa,
    String? berat,
    String? tinggi,
    String? darah,
    String? alamatSiswa,
    int? jarak,
    String? kodePosSiswa,
    String? telponSiswa,
    String? hpSiswa,
    String? emailSiswa,
    String? kesehatan,
    AsalSekolah? asalSekolah,
    String? nomorIjasah,
    String? tanggalIjasah,
    String? keteranganSekolah,
    String? namaAyah,
    String? namaIbu,
    String? statusAyah,
    String? statusIbu,
    String? tempatLahirAyah,
    String? tempatLahirIbu,
    String? tanggalLahirAyah,
    String? tanggalLahirIbu,
    bool? almarhumAyah,
    bool? almarhumIbu,
    TingkatPendidikan? pendidikanAyah,
    TingkatPendidikan? pendidikanIbu,
    JenisPekerjaan? pekerjaanAyah,
    JenisPekerjaan? pekerjaanIbu,
    String? wali,
    int? penghasilanAyah,
    int? penghasilanIbu,
    String? alamatOrtu,
    String? telponOrtu,
    String? hpOrtu,
    String? emailAyah,
    String? emailIbu,
    String? alamatSurat,
    String? keterangan,
    String? hobi,
    bool? frompsb,
    String? ketpsb,
    required bool isAlumni,
    required DateTime createdAt,
  }) = _SiswaDetail;

  factory SiswaDetail.fromJson(Map<String, dynamic> json) =>
      _$SiswaDetailFromJson(json);

  String get fotoCacheKey => "cache-foto-$id";
}
