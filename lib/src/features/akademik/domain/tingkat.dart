import 'package:freezed_annotation/freezed_annotation.dart';

import 'departemen.dart';
import 'pegawai.dart';
import 'tahun_ajaran.dart';

part 'tingkat.freezed.dart';
part 'tingkat.g.dart';

@freezed
class Tingkat with _$Tingkat {
  factory Tingkat({
    required String id,
    required String nama,
    required Departemen departemen,
  }) = _Tingkat;

  factory Tingkat.fromJson(Map<String, dynamic> json) => _$TingkatFromJson(json);
}
