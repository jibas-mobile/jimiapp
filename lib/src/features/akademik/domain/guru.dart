import 'package:freezed_annotation/freezed_annotation.dart';

import 'pelajaran.dart';

part 'guru.freezed.dart';
part 'guru.g.dart';

@freezed
class Guru with _$Guru {
  factory Guru({
    required String id,
    required Pelajaran pelajaran,
  }) = _Guru;

  factory Guru.fromJson(Map<String, dynamic> json) => _$GuruFromJson(json);
}
