import 'package:freezed_annotation/freezed_annotation.dart';

import 'pegawai.dart';
import 'tahun_ajaran.dart';
import 'tingkat.dart';

part 'kelas.freezed.dart';
part 'kelas.g.dart';

@freezed
class Kelas with _$Kelas {
  factory Kelas({
    required String id,
    required String nama,
    required Tingkat tingkat,
    TahunAjaran? tahunAjaran,
    Pegawai? wali,
  }) = _Kelas;

  factory Kelas.fromJson(Map<String, dynamic> json) => _$KelasFromJson(json);
}
