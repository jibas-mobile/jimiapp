import 'package:freezed_annotation/freezed_annotation.dart';

part 'suku.freezed.dart';

part 'suku.g.dart';

@freezed
class Suku with _$Suku {
  factory Suku({
    required String id,
    required String nama,
    String? info1,
    String? info2,
    String? info3,
    DateTime? createdAt,
    int? urutan,
  }) = _Suku;

  factory Suku.fromJson(Map<String, dynamic> json) => _$SukuFromJson(json);
}
