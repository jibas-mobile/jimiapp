import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:jimiapp/src/features/akademik/domain/departemen.dart';

part 'angkatan.freezed.dart';
part 'angkatan.g.dart';

@freezed
class Angkatan with _$Angkatan {
  factory Angkatan({
    required String id,
    required String nama,
    Departemen? departemen,
    bool? aktif,
    String? info1,
    String? info2,
    String? info3,
    String? keterangan,
    DateTime? ts,
  }) = _Angkatan;

  factory Angkatan.fromJson(Map<String, dynamic> json) => _$AngkatanFromJson(json);
}
