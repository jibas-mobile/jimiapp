import 'package:freezed_annotation/freezed_annotation.dart';

import 'departemen.dart';

part 'tahun_ajaran.freezed.dart';
part 'tahun_ajaran.g.dart';

@freezed
class TahunAjaran with _$TahunAjaran {
  factory TahunAjaran({
    required String id,
    required String nama,
    DateTime? tglakhir,
    DateTime? tglmulai,
    Departemen? departemen,
  }) = _TahunAjaran;

  factory TahunAjaran.fromJson(Map<String, dynamic> json) => _$TahunAjaranFromJson(json);
}
