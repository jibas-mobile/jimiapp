import 'package:freezed_annotation/freezed_annotation.dart';

import 'pegawai.dart';

part 'departemen.freezed.dart';
part 'departemen.g.dart';

@freezed
class Departemen with _$Departemen {
  factory Departemen({
    required String id,
    required String nama,
    Pegawai? kepsek,
    int? urutan,
  }) = _Departemen;

  factory Departemen.fromJson(Map<String, dynamic> json) => _$DepartemenFromJson(json);
}
