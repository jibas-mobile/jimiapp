import 'package:freezed_annotation/freezed_annotation.dart';

part 'bagian_pegawai.freezed.dart';
part 'bagian_pegawai.g.dart';

@freezed
class BagianPegawai with _$BagianPegawai {
  factory BagianPegawai({
    required String id,
    required String nama,
  }) = _BagianPegawai;

  factory BagianPegawai.fromJson(Map<String, dynamic> json) => _$BagianPegawaiFromJson(json);
}
