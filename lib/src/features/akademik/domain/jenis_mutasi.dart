import 'package:freezed_annotation/freezed_annotation.dart';

part 'jenis_mutasi.freezed.dart';

part 'jenis_mutasi.g.dart';

@freezed
class JenisMutasi with _$JenisMutasi {
  factory JenisMutasi({
    required String id,
    required String nama,
    String? info1,
    String? info2,
    String? info3,
    String? keterangan,
    DateTime? createdAt,
    int? urutan,
  }) = _JenisMutasi;

  factory JenisMutasi.fromJson(Map<String, dynamic> json) => _$JenisMutasiFromJson(json);
}
