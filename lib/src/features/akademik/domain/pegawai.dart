import 'package:freezed_annotation/freezed_annotation.dart';

import 'bagian_pegawai.dart';
import 'guru.dart';
import 'kelas.dart';

part 'pegawai.freezed.dart';
part 'pegawai.g.dart';

@freezed
class Pegawai with _$Pegawai {
  factory Pegawai({
    required String id,
    required String nama,
    required String nip,
    required BagianPegawai bagian,
    String? email,
    String? handphone,
    String? alamat,
    String? foto,
    String? keterangan,
    Guru? guru,
    List<Kelas>? kelasSet,
  }) = _Pegawai;

  factory Pegawai.fromJson(Map<String, dynamic> json) => _$PegawaiFromJson(json);
}
