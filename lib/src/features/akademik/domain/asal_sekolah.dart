import 'package:freezed_annotation/freezed_annotation.dart';

part 'asal_sekolah.freezed.dart';

part 'asal_sekolah.g.dart';

@freezed
class AsalSekolah with _$AsalSekolah {
  factory AsalSekolah({
    required String id,
    required String nama,
    required String departemen,
    String? info1,
    String? info2,
    String? info3,
    DateTime? createdAt,
    int? urutan,
  }) = _AsalSekolah;

  factory AsalSekolah.fromJson(Map<String, dynamic> json) => _$AsalSekolahFromJson(json);
}
