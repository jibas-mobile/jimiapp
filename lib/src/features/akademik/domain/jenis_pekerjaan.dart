import 'package:freezed_annotation/freezed_annotation.dart';

part 'jenis_pekerjaan.freezed.dart';

part 'jenis_pekerjaan.g.dart';

@freezed
class JenisPekerjaan with _$JenisPekerjaan {
  factory JenisPekerjaan({
    required String id,
    required String nama,
    String? info1,
    String? info2,
    String? info3,
    DateTime? createdAt,
    int? urutan,
  }) = _JenisPekerjaan;

  factory JenisPekerjaan.fromJson(Map<String, dynamic> json) => _$JenisPekerjaanFromJson(json);
}
