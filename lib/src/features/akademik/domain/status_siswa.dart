import 'package:freezed_annotation/freezed_annotation.dart';

part 'status_siswa.freezed.dart';

part 'status_siswa.g.dart';

@freezed
class StatusSiswa with _$StatusSiswa {
  factory StatusSiswa({
    required String id,
    required String nama,
    String? info1,
    String? info2,
    String? info3,
    DateTime? createdAt,
    int? urutan,
  }) = _StatusSiswa;

  factory StatusSiswa.fromJson(Map<String, dynamic> json) => _$StatusSiswaFromJson(json);
}
