import 'package:freezed_annotation/freezed_annotation.dart';

part 'tingkat_pendidikan.freezed.dart';

part 'tingkat_pendidikan.g.dart';

@freezed
class TingkatPendidikan with _$TingkatPendidikan {
  factory TingkatPendidikan({
    required String id,
    required String nama,
    String? info1,
    String? info2,
    String? info3,
    DateTime? createdAt,
    int? urutan,
  }) = _TingkatPendidikan;

  factory TingkatPendidikan.fromJson(Map<String, dynamic> json) => _$TingkatPendidikanFromJson(json);
}
