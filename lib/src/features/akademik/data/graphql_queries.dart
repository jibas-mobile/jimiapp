abstract class Queries {
  static const String siswaByID = r'''
query 
	findSiswaByID($id: ID!) {
		siswa(id: $id) {
			id
			nama
			kelas {
				id
				nama
				tahunAjaran {
				  id
				  nama
				}
				tingkat {
					id
					nama
					departemen {
						id
						nama
					}
				}
			}
			nis
			nisn
			nik
			noun
			panggilan
			aktif
			tahunMasuk
			angkatan {
				id
				nama
				departemen {
					id
					nama
				}
			}
			suku {
				id
				nama
			}
			agama {
				id
				nama
			}
			kondisi {
				id
				nama
			}
			status {
				id
				nama
			}
			fotoUrl
			kelamin
			tempatLahir
			tanggalLahir
			warga
			anakKe
			jumlahSaudara
			statusAnak
			jumlahSaudaraKandung
			jumlahSaudaraTiri
			bahasa
			berat
			tinggi
			darah
			alamatSiswa
			jarak
			kodePosSiswa
			telponSiswa
			hpSiswa
			emailSiswa
			kesehatan
			asalSekolah {
				id
				nama
				departemen
			}
			nomorIjasah
			tanggalIjasah
			keteranganSekolah
			namaAyah
			namaIbu
			statusAyah
			statusIbu
			tempatLahirAyah
			tempatLahirIbu
			tanggalLahirAyah
			tanggalLahirIbu
			almarhumAyah
			almarhumIbu
			pendidikanAyah {
				id
				nama
			}
			pendidikanIbu {
				id
				nama
			}
			pekerjaanAyah {
				id
				nama
			}
			pekerjaanIbu {
				id
				nama
			}
			wali
			penghasilanAyah
			penghasilanIbu
			alamatOrtu
			telponOrtu
			hpOrtu
			emailAyah
			emailIbu
			alamatSurat
			keterangan
			hobi
			frompsb
			ketpsb
			isAlumni
			createdAt
		}
	}
  ''';
}