import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:jimiapp/src/constants/common.dart';

import '../../../providers/fetch_policy_provider.dart';
import '../../../services/graphql_service.dart';
import '../../../services/organisasi_service.dart';
import '../../../services/web_service.dart';
import '../../login/application/auth_service.dart';
import '../../login/domain/organisasi.dart';
import '../domain/siswa_detail.dart';
import 'graphql_queries.dart';

final siswaDetailByIDProvider =
    FutureProvider.autoDispose.family<SiswaDetail, String>((ref, id) async {
  final akademiRepositry = await ref.watch(akademiRepositoryProvider.future);
  final fetchPolicy = ref.read(fetchPolicyProvider);
  ref.maintainState = true;
  return akademiRepositry.findSiswaDetailByID(id, policy: fetchPolicy);
});

final akademiRepositoryProvider =
    FutureProvider<AkademikRepository>((ref) async {
  final graphQLClient = await ref.watch(graphQLClientProvider.future);
  final dio = ref.watch(dioProvider);
  final authKey = await ref.watch(getAuthKeyProvider.future);
  final organisasi = await ref.watch(currentOrganisasiProvider.future);
  return AkademikRepository(
      graphQLClient: graphQLClient,
      dio: dio,
      authKey: authKey!,
      organisasi: organisasi!);
});

class AkademikRepository {
  AkademikRepository({
    required this.graphQLClient,
    required this.dio,
    required this.authKey,
    required this.organisasi,
  });

  final GraphQLClient graphQLClient;
  final Dio dio;
  final String authKey;
  final Organisasi organisasi;

  Future<SiswaDetail> findSiswaDetailByID(String id,
      {FetchPolicy policy = FetchPolicy.cacheFirst}) async {
    final options = QueryOptions(
        document: gql(Queries.siswaByID),
        fetchPolicy: policy,
        variables: <String, String>{
          'id': id,
        });
    final result = await graphQLClient.query(options);
    final siswa = SiswaDetail.fromJson(result.data!['siswa']);
    return siswa;
  }

  Future<void> uploadPhotoSiswa(String id, Stream<Uint8List> pic,
      {required int size,
      String? mimeType,
      ProgressCallback? onSendProgress}) async {
    final url = '${organisasi.apiUrl}/api/photo/siswa/$id';
    final Map<String, dynamic> headers = {
      'Content-Disposition': 'attachment; filename=upload.jpg',
      'Authorization': 'Bearer $authKey',
      'Accept': "*/*",
      'Connection': 'keep-alive',
      'User-Agent': CommonConst.userAgent,
      'Content-Length': size.toString(),
    };
    final options = Options(
      headers: headers,
      contentType: mimeType ??
          'image/jpeg', // doesn't matter, the server will detect when downloading
    );

    final response = await dio.post(
      url,
      data: pic,
      onSendProgress: onSendProgress,
      options: options,
    );
  }
}
