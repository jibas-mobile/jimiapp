import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../common_widgets/rev_list_tile.dart';
import '../../domain/kelas.dart';

class KelasDetailPage extends ConsumerWidget {
  const KelasDetailPage({Key? key, required this.kelas}) : super(key: key);

  final Kelas kelas;

  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: () async {},
        child: CustomScrollView(
          physics: const BouncingScrollPhysics(
              parent: AlwaysScrollableScrollPhysics()),
          slivers: [
            SliverAppBar(
              title: Text(
                  '${kelas.tingkat.nama} ${kelas.nama} - ${kelas.tingkat.departemen.nama}'),
              // actions: [
              //   // Absensi
              //   IconButton(
              //       onPressed: () {}, icon: Icon(Icons.calendar_today_rounded))
              // ],
            ),
            SliverList(
              delegate: SliverChildListDelegate(<Widget>[
                RevListTile(
                  value: kelas.nama,
                  title: 'Kelas',
                ),
                RevListTile(
                  value: kelas.tingkat.nama,
                  title: 'Tingkat',
                ),
                // TODO link to guru
                RevListTile(
                  title: 'Wali',
                  value: kelas.wali?.nama,
                ),
                // TODO kapasitas dll
                // TODO link to siswa
              ]),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {},
        label: const Text('Absensi'),
        icon: const Icon(Icons.calendar_today_rounded),
      ),
    );
  }
}
