import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../common_widgets/separasi_baris.dart';
import '../../../../utils/top_providers.dart';
import '../../../search/presentation/list_tile_siswa.dart';
import '../../domain/siswa.dart';

class SiswaDetailError extends ConsumerWidget {
  const SiswaDetailError({
    Key? key,
    required this.siswa,
    required this.onRetry,
    required this.error,
    required this.stack,
  }) : super(key: key);

  final Siswa siswa;
  final void Function()? onRetry;
  final Object? error;
  final StackTrace? stack;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final logger = ref.read(loggerProvider);
    logger.d(error);
    logger.d(stack);

    return Scaffold(
        appBar: AppBar(
          title: Hero(
              tag: ListTileSiswa.createHeroTag(siswa.id),
              child: Text(
                siswa.nama,
                style: Theme.of(context).textTheme.headline6,
              )),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.error,
                size: 48.0,
                color: Theme.of(context).errorColor,
              ),
              Text(
                'Terjadi kesalahan ketika mengambil data',
                style: Theme.of(context).textTheme.subtitle1,
              ),
              const SeparasiBaris(),
              ElevatedButton(onPressed: onRetry, child: Text('Coba lagi')),
            ],
          ),
        ));
  }
}
