import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../common_widgets/app_bar_profile_image.dart';
import '../../../../common_widgets/col_list_title.dart';
import '../../../../common_widgets/head_list_tile.dart';
import '../../../../common_widgets/rev_list_tile.dart';
import '../../../../common_widgets/take_picture_icon.dart';
import '../../../../common_widgets/two_column.dart';
import '../../../../services/organisasi_service.dart';
import '../../../../utils/top_providers.dart';
import '../../../search/presentation/list_tile_siswa.dart';
import '../../application/upload_picture_siswa.dart';
import '../../domain/siswa_detail.dart';
import 'upload_pic_siswa_dialog.dart';

class SiswaDetailPageScrollView extends ConsumerStatefulWidget {
  const SiswaDetailPageScrollView({
    Key? key,
    required this.siswa,
    required this.onRefresh,
  }) : super(key: key);

  final SiswaDetail siswa;
  final RefreshCallback onRefresh;

  @override
  _SiswaDetailPageScrollViewState createState() =>
      _SiswaDetailPageScrollViewState();
}

class _SiswaDetailPageScrollViewState
    extends ConsumerState<SiswaDetailPageScrollView> {
  @override
  Widget build(BuildContext context) {
    final logger = ref.read(loggerProvider);
    final organisasi = ref.watch(currentOrganisasiProvider).value!;
    final fotoUrl = "${organisasi.apiUrl}${widget.siswa.fotoUrl}";
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: widget.onRefresh,
        child: CustomScrollView(
          physics: const BouncingScrollPhysics(
              parent: AlwaysScrollableScrollPhysics()),
          slivers: <Widget>[
            SliverAppBar(
              actions: [
                TakePictureIcon(onSelected: (file) async {
                  if (file == null) {
                    logger.d('Got no image');
                    return;
                  }
                  final uploadInfo = UploadSiswaInfo(
                    id: widget.siswa.nis,
                    xFile: file,
                  );
                  final uploadPictureSiswaNotifier =
                      ref.read(uploadPictureSiswaProvider(uploadInfo).notifier);

                  try {
                    final fut1 = showDialog(
                        context: context,
                        builder: (context) => UploadPicSiswaDialog(
                              uploadSiswaInfo: uploadInfo,
                            ));
                    final fut2 = uploadPictureSiswaNotifier.startUpload();
                    await Future.wait([fut1, fut2]);
                    if (fotoUrl != null) {
                      await CachedNetworkImage.evictFromCache(fotoUrl);
                    }
                  } catch (e) {
                    logger.d(e);
                  }
                }),
                // IconButton(onPressed: () {}, icon: const Icon(Icons.edit)), // TODO edit siswa
              ],
              floating: false,
              pinned: true,
              snap: false,
              stretch: true,
              expandedHeight: 300.0,
              flexibleSpace: FlexibleSpaceBar(
                stretchModes: const <StretchMode>[
                  StretchMode.zoomBackground,
                  StretchMode.blurBackground,
                  StretchMode.fadeTitle,
                ],
                centerTitle: false,
                title: Hero(
                    tag: ListTileSiswa.createHeroTag(widget.siswa.id),
                    child: Text(
                      widget.siswa.nama,
                      style: Theme.of(context).appBarTheme.titleTextStyle,
                    )),
                background: Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    AppBarProfileImage(
                      url: fotoUrl,
                      cacheKey: widget.siswa.fotoCacheKey,
                    ),
                    const DecoratedBox(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment(0.0, 0.5),
                          end: Alignment.center,
                          colors: <Color>[
                            Color(0x60000000),
                            Color(0x00000000),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate(
                <Widget>[
                  RevListTile(
                    title: 'siswa_detail_page.departemen'.tr(),
                    value: widget.siswa.kelas.tingkat.departemen.nama,
                  ),
                  RevListTile(
                    title: 'siswa_detail_page.thn_ajaran'.tr(),
                    value: widget.siswa.kelas.tahunAjaran!.nama,
                  ),
                  RevListTile(
                    title: 'siswa_detail_page.kelas'.tr(),
                    value:
                        '${widget.siswa.kelas.tingkat.nama} - ${widget.siswa.kelas.nama}',
                  ),
                  RevListTile(
                    title: 'siswa_detail_page.nis'.tr(),
                    value: widget.siswa.nis,
                  ),
                  HeadListTile(
                      'siswa_detail_page.data_pribadi_siswa.judul'.tr()),
                  RevListTile(
                    title: 'siswa_detail_page.data_pribadi_siswa.nisn'.tr(),
                    value: widget.siswa.nisn,
                  ),
                  RevListTile(
                    title: 'siswa_detail_page.data_pribadi_siswa.nik'.tr(),
                    value: widget.siswa.nik,
                  ),
                  RevListTile(
                    title: 'siswa_detail_page.data_pribadi_siswa.noun'.tr(),
                    value: widget.siswa.noun,
                  ),
                  RevListTile(
                    title:
                        'siswa_detail_page.data_pribadi_siswa.nama_siswa'.tr(),
                    value: widget.siswa.nama,
                  ),
                  RevListTile(
                    title: 'siswa_detail_page.data_pribadi_siswa.nama_panggilan'
                        .tr(),
                    value: widget.siswa.panggilan,
                  ),
                  RevListTile(
                    title: 'siswa_detail_page.data_pribadi_siswa.jenis_kelamin'
                        .tr(),
                    value: (widget.siswa.kelamin == null)
                        ? '-'
                        : 'common.gender'
                            .tr(gender: widget.siswa.kelamin!.toEnglishValue()),
                  ),
                  RevListTile(
                    title: 'siswa_detail_page.data_pribadi_siswa.tempat_lahir'
                        .tr(),
                    value: widget.siswa.tempatLahir,
                  ),
                  RevListTile(
                    title: 'siswa_detail_page.data_pribadi_siswa.tanggal_lahir'
                        .tr(),
                    value: (widget.siswa.tanggalLahir == null)
                        ? null
                        : DateFormat.yMMMd().format(widget.siswa.tanggalLahir!),
                  ),
                  RevListTile(
                    title: 'siswa_detail_page.data_pribadi_siswa.agama'.tr(),
                    value: widget.siswa.agama?.nama,
                  ),
                  RevListTile(
                    title:
                        'siswa_detail_page.data_pribadi_siswa.warganegara'.tr(),
                    value: widget.siswa.warga,
                  ),
                  RevListTile(
                    title: 'siswa_detail_page.data_pribadi_siswa.anakke'.tr(),
                    value: widget.siswa.anakKe?.toString(),
                  ),
                  RevListTile(
                    title: 'siswa_detail_page.data_pribadi_siswa.dari'.tr(),
                    value: (widget.siswa.jumlahSaudara == null)
                        ? null
                        : 'siswa_detail_page.data_pribadi_siswa.bersaudara'
                            .plural(widget.siswa.jumlahSaudara!),
                  ),
                  RevListTile(
                    title:
                        'siswa_detail_page.data_pribadi_siswa.status_anak'.tr(),
                    value: widget.siswa.statusAnak,
                  ),
                  RevListTile(
                    title:
                        'siswa_detail_page.data_pribadi_siswa.jumlah_sdr_kandung'
                            .tr(),
                    value: (widget.siswa.jumlahSaudaraKandung == null)
                        ? null
                        : 'siswa_detail_page.data_pribadi_siswa.jumlah_orang'
                            .plural(widget.siswa.jumlahSaudaraKandung!),
                  ),
                  RevListTile(
                    title:
                        'siswa_detail_page.data_pribadi_siswa.jumlah_sdr_tiri'
                            .tr(),
                    value: (widget.siswa.jumlahSaudaraTiri == null)
                        ? null
                        : 'siswa_detail_page.data_pribadi_siswa.jumlah_orang'
                            .plural(widget.siswa.jumlahSaudaraTiri!),
                  ),
                  RevListTile(
                      title:
                          'siswa_detail_page.data_pribadi_siswa.kondisi_siswa'
                              .tr(),
                      value: widget.siswa.kondisiSiswa?.nama),
                  RevListTile(
                      title: 'siswa_detail_page.data_pribadi_siswa.status_siswa'
                          .tr(),
                      value: widget.siswa.status?.nama),
                  RevListTile(
                      title:
                          'siswa_detail_page.data_pribadi_siswa.bahasa_sehari'
                              .tr(),
                      value: widget.siswa.bahasa),

                  HeadListTile(
                      'siswa_detail_page.ket_tempat_tinggal.judul'.tr()),
                  RevListTile(
                      title: 'siswa_detail_page.ket_tempat_tinggal.alamat'.tr(),
                      value: widget.siswa.alamatSiswa),
                  RevListTile(
                      title:
                          'siswa_detail_page.ket_tempat_tinggal.kode_pos'.tr(),
                      value: widget.siswa.kodePosSiswa),
                  RevListTile(
                      title:
                          'siswa_detail_page.ket_tempat_tinggal.jarak_ke_sekolah'
                              .tr(),
                      value: "${widget.siswa.jarak} km"),
                  RevListTile(
                      title:
                          'siswa_detail_page.ket_tempat_tinggal.telepon'.tr(),
                      value: widget.siswa.telponSiswa),
                  RevListTile(
                      title:
                          'siswa_detail_page.ket_tempat_tinggal.handphone'.tr(),
                      value: widget.siswa.hpSiswa),
                  RevListTile(
                      title: 'siswa_detail_page.ket_tempat_tinggal.email'.tr(),
                      value: widget.siswa.emailSiswa),

                  HeadListTile('siswa_detail_page.ket_kesehatan.judul'.tr()),
                  RevListTile(
                      title: 'siswa_detail_page.ket_kesehatan.berat_badan'.tr(),
                      value: widget.siswa.berat),
                  RevListTile(
                      title:
                          'siswa_detail_page.ket_kesehatan.tinggi_badan'.tr(),
                      value: widget.siswa.tinggi),
                  RevListTile(
                      title: 'siswa_detail_page.ket_kesehatan.gol_darah'.tr(),
                      value: widget.siswa.darah),
                  RevListTile(
                      title: 'siswa_detail_page.ket_kesehatan.riwayat_penyakit'
                          .tr(),
                      value: widget.siswa.kesehatan),

                  HeadListTile(
                      'siswa_detail_page.ket_pendidikan_sblm.judul'.tr()),
                  RevListTile(
                      title:
                          'siswa_detail_page.ket_pendidikan_sblm.asal_sekolah'
                              .tr(),
                      value: widget.siswa.asalSekolah?.nama),
                  RevListTile(
                      title: 'siswa_detail_page.ket_pendidikan_sblm.no_ijasah'
                          .tr(),
                      value: widget.siswa.nomorIjasah),
                  RevListTile(
                      title: 'siswa_detail_page.ket_pendidikan_sblm.tgl_ijasah'
                          .tr(),
                      value: widget.siswa.tanggalIjasah),
                  RevListTile(
                      title:
                          'siswa_detail_page.ket_pendidikan_sblm.ket_pendidikan_sblm_ket'
                              .tr(),
                      value: widget.siswa.keteranganSekolah),

                  HeadListTile('siswa_detail_page.ket_ortu.judul'.tr()),
                  TwoColumn(
                      Expanded(
                          child: Text(
                        'siswa_detail_page.ket_ortu.ayah'.tr(),
                        style: Theme.of(context).textTheme.titleLarge,
                      )),
                      Expanded(
                          child: Text('siswa_detail_page.ket_ortu.ibu'.tr(),
                              style: Theme.of(context).textTheme.titleLarge))),
                  TwoColumn(
                    ColListTile(
                      title: 'siswa_detail_page.ket_ortu.nama'.tr(),
                      value: widget.siswa.namaAyah,
                    ),
                    ColListTile(
                      title: 'siswa_detail_page.ket_ortu.nama'.tr(),
                      value: widget.siswa.namaIbu,
                    ),
                  ),
                  TwoColumn(
                    ColListTile(
                      title: 'siswa_detail_page.ket_ortu.status'.tr(),
                      value: widget.siswa.statusAyah,
                    ),
                    ColListTile(
                      title: 'siswa_detail_page.ket_ortu.status'.tr(),
                      value: widget.siswa.statusIbu,
                    ),
                  ),
                  TwoColumn(
                    ColListTile(
                      title: 'siswa_detail_page.data_pribadi_siswa.tempat_lahir'
                          .tr(),
                      value: widget.siswa.tempatLahirAyah,
                    ),
                    ColListTile(
                      title: 'siswa_detail_page.data_pribadi_siswa.tempat_lahir'
                          .tr(),
                      value: widget.siswa.tempatLahirIbu,
                    ),
                  ),
                  TwoColumn(
                    ColListTile(
                      title:
                          'siswa_detail_page.data_pribadi_siswa.tanggal_lahir'
                              .tr(),
                      value: widget.siswa.tanggalLahirAyah,
                    ),
                    ColListTile(
                      title:
                          'siswa_detail_page.data_pribadi_siswa.tanggal_lahir'
                              .tr(),
                      value: widget.siswa.tanggalLahirIbu,
                    ),
                  ),
                  TwoColumn(
                    ColListTile(
                      title: 'siswa_detail_page.ket_ortu.pendidikan'.tr(),
                      value: widget.siswa.pendidikanAyah?.nama,
                    ),
                    ColListTile(
                      title: 'siswa_detail_page.ket_ortu.pendidikan'.tr(),
                      value: widget.siswa.pendidikanIbu?.nama,
                    ),
                  ),
                  TwoColumn(
                    ColListTile(
                      title: 'siswa_detail_page.ket_ortu.pekerjaan'.tr(),
                      value: widget.siswa.pekerjaanAyah?.nama,
                    ),
                    ColListTile(
                      title: 'siswa_detail_page.ket_ortu.pekerjaan'.tr(),
                      value: widget.siswa.pekerjaanIbu?.nama,
                    ),
                  ),
                  TwoColumn(
                    ColListTile(
                      title: 'siswa_detail_page.ket_ortu.penghasilan'.tr(),
                      value: widget.siswa.penghasilanAyah.toString(),
                    ),
                    ColListTile(
                      title: 'siswa_detail_page.ket_ortu.penghasilan'.tr(),
                      value: widget.siswa.penghasilanIbu?.toString(),
                    ),
                  ),
                  TwoColumn(
                    ColListTile(
                      title:
                          'siswa_detail_page.ket_tempat_tinggal.telepon'.tr(),
                      value: widget.siswa.telponOrtu,
                    ),
                    const ColListTile(
                      title: '',
                      value: null,
                    ),
                  ),

                  TwoColumn(
                    ColListTile(
                      title: 'siswa_detail_page.ket_ortu.handphone1'.tr(),
                      value: widget.siswa.hpOrtu,
                    ),
                    const ColListTile(
                      title: '',
                      value: '',
                    ),
                  ),

                  HeadListTile('siswa_detail_page.ket_lainnya.judul'.tr()),
                  RevListTile(
                      title: 'siswa_detail_page.ket_lainnya.hobi'.tr(),
                      value: widget.siswa.hobi),
                  RevListTile(
                      title: 'siswa_detail_page.ket_lainnya.alamat_surat'.tr(),
                      value: widget.siswa.alamatSurat),
                  RevListTile(
                      title: 'siswa_detail_page.ket_lainnya.ket'.tr(),
                      value: widget.siswa.keterangan),
                  // ListTiles++
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
