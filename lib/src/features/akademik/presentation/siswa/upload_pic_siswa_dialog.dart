import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../application/upload_picture_siswa.dart';

class UploadPicSiswaDialog extends ConsumerStatefulWidget {
  const UploadPicSiswaDialog({
    Key? key,
    required this.uploadSiswaInfo,
  }) : super(key: key);

  final UploadSiswaInfo uploadSiswaInfo;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => UploadPicSiswaState();
}

class UploadPicSiswaState extends ConsumerState<UploadPicSiswaDialog> {
  void _btnDone(context) {
    Navigator.pop(context);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final uploadProgress =
        ref.watch(uploadPictureSiswaProvider(widget.uploadSiswaInfo));

    return SimpleDialog(
      title: Text('Uploading file ${uploadProgress.progress}%'),
      children: [
        Visibility(
          visible: uploadProgress.done,
          child: SimpleDialogOption(
            onPressed: uploadProgress.done ? () => _btnDone(context) : null,
            child: const Text('Uda deh'),
          ),
        ),
      ],
    );
  }
}
