import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:jimiapp/src/providers/cache_manager_provider.dart';

import '../../../../common_widgets/app_bar_profile_image.dart';
import '../../../../common_widgets/loading_page.dart';
import '../../../../providers/fetch_policy_provider.dart';
import '../../../../utils/top_providers.dart';
import '../../../search/presentation/list_tile_siswa.dart';
import '../../data/akademik_repository.dart';
import '../../domain/siswa.dart';
import 'siswa_detail_error.dart';
import 'siswa_detail_page_scroll_view.dart';

class SiswaDetailPage extends ConsumerWidget {
  const SiswaDetailPage({Key? key, required this.siswa}) : super(key: key);
  final Siswa siswa;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final siswaDetailAsyncValue = ref.watch(siswaDetailByIDProvider(siswa.id));
    final cacheManager = ref.watch(cacheManagerProvider);
    return siswaDetailAsyncValue.when(
        data: (siswaDetail) => SiswaDetailPageScrollView(
              siswa: siswaDetail,
              onRefresh: () async {
                ref.read(loggerProvider).d('cuy refresh nih coy');
                final fetchPolicy = ref.watch(fetchPolicyProvider.notifier);
                fetchPolicy.state = FetchPolicy.networkOnly;
                ref.refresh(siswaDetailByIDProvider(siswa.id));
                await ref.watch(siswaDetailByIDProvider(siswa.id).future);
                fetchPolicy.state = FetchPolicy.cacheFirst;

                await CachedNetworkImage.evictFromCache(siswaDetail.fotoUrl!,
                    cacheManager: cacheManager,
                    cacheKey: siswaDetail.fotoCacheKey);
                ref.refresh(
                    refreshAppBarProfileImageProvider(siswaDetail.fotoUrl!));
              },
            ),
        error: (err, stack) => SiswaDetailError(
              siswa: siswa,
              error: err,
              stack: stack,
              onRetry: () {
                ref.refresh(siswaDetailByIDProvider(siswa.id));
              },
            ),
        loading: () => LoadingPage(
            title: Hero(
                tag: ListTileSiswa.createHeroTag(siswa.id),
                child: Text(
                  siswa.nama,
                  style: Theme.of(context).textTheme.headline6,
                ))));
  }
}
