import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../services/secure_storage_service.dart';
import '../../../services/web_service.dart';

final userLoggedInProvider = FutureProvider<bool>((ref) async {
  final jimiAuth = ref.watch(authServiceProvider);
  return jimiAuth.isLoggedIn();
});

final getAuthKeyProvider = FutureProvider<String?>((ref) {
  final secureStorageService = ref.watch(secureStorageServiceProvider);
  return secureStorageService.getAuthKey();
});

final authServiceProvider = Provider<AuthService>((ref) {
  final webService = ref.watch(authRepositoryProvider);
  final secureStorageService = ref.watch(secureStorageServiceProvider);
  return AuthService(webService, secureStorageService);
});

class AuthService {
  AuthService(this.webService, this.secureStorageService);

  final AuthWebRepository webService;
  final SecureStorageService secureStorageService;

  Future<void> loginPegawai(String username, password) async {
    final organisasi = await secureStorageService.getOrganisasi();
    if (organisasi == null) {
      throw LoginException('no organisation has been selected');
    }
    try {
      final key =
      await webService.loginPegawai(organisasi.apiUrl, username, password);
      await secureStorageService.setAuthKey(key);
    } catch (e) {
      rethrow;
    }
  }

  Future<void> logout() async {
    await secureStorageService.clearAuthKey();
  }

  Future<bool> isLoggedIn() async {
    final key = await secureStorageService.getAuthKey();
    return key != null;
  }
}
