import 'package:validators/sanitizers.dart';

class Organisasi {
  final String nama;
  final bool aktif;
  final String apiUrl;
  final String? logoUrl;
  final String? websiteUrl;

  Organisasi({
    required this.nama,
    required this.aktif,
    required this.apiUrl,
    this.logoUrl,
    this.websiteUrl
  });

  factory Organisasi.fromJson(Map<String, dynamic> json) {
    final nama = json['nama'] as String;
    final aktif = json['aktif'] as bool;
    final apiUrl = rtrim(json['apiUrl'] as String, '/');
    final logoUrl = json['logoUrl'] as String?;
    final websiteUrl = json['websiteUrl'] as String?;
    return Organisasi(
      nama: nama,
      aktif: aktif,
      apiUrl: apiUrl,
      logoUrl: logoUrl,
      websiteUrl: websiteUrl
    );
  }

  Map<String, dynamic> toJson() => {
    'nama': nama,
    'aktif': aktif,
    'apiUrl': apiUrl,
    'logoUrl': logoUrl,
    'websiteUrl': websiteUrl,
  };
}