import 'package:freezed_annotation/freezed_annotation.dart';

import '../../akademik/domain/pegawai.dart';

part 'jimi_user.freezed.dart';
part 'jimi_user.g.dart';

@freezed
class JimiUser with _$JimiUser {
  factory JimiUser({
    required String id,
    required String jibasLogin,
    required String type,
    Pegawai? pegawai,
  }) = _JimiUser;

  factory JimiUser.fromJson(Map<String, dynamic> json) => _$JimiUserFromJson(json);
}
