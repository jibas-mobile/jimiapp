import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:form_validator/form_validator.dart';
import "package:rounded_loading_button/rounded_loading_button.dart";

import 'login_view_model.dart';
import '../../../common_widgets/separasi_baris.dart';
import '../../../constants/values.dart';
import '../../../services/organisasi_service.dart';

class LoginForm extends ConsumerStatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => LoginFormState();
}

class LoginFormState extends ConsumerState<LoginForm> {
  final RoundedLoadingButtonController _btnController =
      RoundedLoadingButtonController();
  final _formKey = GlobalKey<FormState>();
  late final _validatorUsername =
      ValidationBuilder(requiredMessage: 'username is required').build();
  late final _validatorPassword =
      ValidationBuilder(requiredMessage: 'password is required').build();

  String _username = '';
  String _password = '';
  bool _isLoading = false;
  bool _loggedIn = false;
  bool _enableButton = false;
  // for now
  // final UserType _userType = UserType.parent;

  void _login() async {
    if (_formKey.currentState!.validate()) {
      final authState = ref.read(authStateProvider.notifier);
      authState.loginPegawai(_username, _password);
    }
  }

  @override
  Widget build(BuildContext context) {
    ref.listen<AsyncValue<bool>>(
        authStateProvider,
        (_, state) => state.whenOrNull(data: (loggedIn) async {
              if (loggedIn) {
                _btnController.success();
                setState(() {
                  _loggedIn = true;
                });
                await Future.delayed(ActionDurations.afterClickDuration);
              }
            }, error: (error, _) async {
              _btnController.error();
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(content: Text(error.toString())),
              );
              await Future.delayed(ActionDurations.afterClickDuration);
              _btnController.reset();
            }));

    final userState = ref.watch(authStateProvider);
    setState(() {
      _isLoading = userState is AsyncLoading<bool>;
    });

    final orgService = ref.watch(organisasiServiceProvider.notifier);
    final organisasiNama = orgService.getOrganisasi?.nama ?? '';
    return Scaffold(
      appBar: AppBar(
        title: Text(organisasiNama),
      ),
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(30.0),
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Form(
                  key: _formKey,
                  onChanged: () => setState(() {
                    _enableButton = _formKey.currentState!.validate();
                  }),
                  child: Column(
                    children: [
                      TextFormField(
                        decoration: const InputDecoration(
                          labelText: 'Username',
                        ),
                        onChanged: (username) => setState(() {
                          _username = username;
                        }),
                        validator: _validatorUsername,
                        readOnly: _isLoading || _loggedIn,
                      ),
                      const SizedBox(height: 16.0),
                      TextFormField(
                        decoration: const InputDecoration(
                          labelText: 'Password',
                        ),
                        onChanged: (password) => setState(() {
                          _password = password;
                        }),
                        obscureText: true,
                        keyboardType: TextInputType.visiblePassword,
                        validator: _validatorPassword,
                        readOnly: _isLoading || _loggedIn,
                        onFieldSubmitted: (_) => _login,
                      ),
                      const SizedBox(height: 16.0),
                    ],
                  ),
                ),
                const SeparasiBaris(),
                RoundedLoadingButton(
                  color: Theme.of(context).primaryColor,
                  controller: _btnController,
                  onPressed: (_enableButton && !_isLoading && !_loggedIn)
                      ? _login
                      : null,
                  child: const Text('Login'),
                  resetAfterDuration: false,
                ),
                const SeparasiBaris(),
                TextButton(
                    onPressed: () async {
                      await orgService.clearOrganisasi();
                      ref.refresh(currentOrganisasiProvider);
                    },
                    child: Text('Ganti organisasi'))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
