import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:form_validator/form_validator.dart';
import 'package:jimiapp/src/utils/top_providers.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';

import '../../../common_widgets/separasi_baris.dart';
import '../../../constants/values.dart';
import '../../../services/organisasi_service.dart';
import '../domain/organisasi.dart';

class OrganisationForm extends ConsumerStatefulWidget {
  const OrganisationForm({Key? key}) : super(key: key);

  @override
  OrganisationFormState createState() => OrganisationFormState();
}

class OrganisationFormState extends ConsumerState<OrganisationForm> {
  final RoundedLoadingButtonController _btnController =
      RoundedLoadingButtonController();

  String _kodeOrganisasi = '';
  bool _isLoading = false;
  bool _hasOrg = false;
  bool _enableButton = false;
  late final _validatorKode =
      ValidationBuilder(requiredMessage: tr('organisation_form.required_msg'))
          .minLength(4, tr('organisation_form.min_len_msg'))
          .build();
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  void _selectOrganisasi() async {
    if (_formKey.currentState!.validate()) {
      final organisasiService = ref.watch(organisasiServiceProvider.notifier);
      await organisasiService.selectOrganisasi(_kodeOrganisasi);
    }
  }

  @override
  Widget build(BuildContext context) {
    final logger = ref.watch(loggerProvider);
    ref.listen<AsyncValue<Organisasi?>>(
        organisasiServiceProvider,
        (_, state) => state.whenOrNull(data: (org) async {
              if (org != null) {
                _btnController.success();
                setState(() {
                  _hasOrg = true;
                });
                await Future.delayed(ActionDurations.afterClickDuration);
                ref.refresh(currentOrganisasiProvider);
              }
            }, error: (error, _) async {
              logger.d(error);
              _btnController.error();
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                    content: Text(
                  error.toString(),
                  overflow: TextOverflow.ellipsis,
                )),
              );
              await Future.delayed(ActionDurations.afterClickDuration);
              _btnController.reset();
            }));
    final organisasiState = ref.watch(organisasiServiceProvider);
    setState(() {
      _isLoading = organisasiState is AsyncLoading<Organisasi?>;
    });

    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(30.0),
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Form(
                  key: _formKey,
                  onChanged: () => setState(() {
                    _enableButton = _formKey.currentState!.validate();
                  }),
                  child: TextFormField(
                    textInputAction: TextInputAction.go,
                    validator: _validatorKode,
                    key: const ValueKey("inputOrgKey"),
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      labelText: tr('organisation_form.textfield_label'),
                    ),
                    onChanged: (name) => setState(() {
                      _kodeOrganisasi = name.toString();
                    }),
                    readOnly: _isLoading || _hasOrg,
                  ),
                ),
                const SeparasiBaris(),
                RoundedLoadingButton(
                  color: Theme.of(context).primaryColor,
                  controller: _btnController,
                  onPressed: (_enableButton && !_isLoading && !_hasOrg)
                      ? _selectOrganisasi
                      : null,
                  child: const Text('organisation_form.enter_btn').tr(),
                  resetAfterDuration: false,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
