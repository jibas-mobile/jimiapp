import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../../constants/values.dart';

import '../application/auth_service.dart';

final authStateProvider =
    StateNotifierProvider<AuthViewModel, AsyncValue<bool>>((ref) {
  final loggedIn = ref.watch(userLoggedInProvider);
  return AuthViewModel(ref, loggedIn);
});

class AuthViewModel extends StateNotifier<AsyncValue<bool>> {
  AuthViewModel(this.ref, AsyncValue<bool> loggedIn) : super(loggedIn);

  final Ref ref;

  Future<void> loginPegawai(String username, password,
      {Duration? delayAfterSuccessLogin, delayAfterFailedLogin}) async {
    try {
      state = const AsyncValue.loading();
      await ref.read(authServiceProvider).loginPegawai(username, password);
      state = const AsyncValue.data(true);
      await Future.delayed(
          delayAfterSuccessLogin ?? ActionDurations.afterClickDuration);
      ref.refresh(userLoggedInProvider);
    } catch (e) {
      state = AsyncValue.error(e.toString());
      if (delayAfterFailedLogin != null) {
        await Future.delayed(delayAfterFailedLogin);
        await Future.delayed(
            delayAfterFailedLogin ?? ActionDurations.afterClickDuration);
      }
      // state = const AsyncValue.data(false);
    }
  }

  Future<void> logout({Duration? delay}) async {
    await ref.read(authServiceProvider).logout();
    state = const AsyncValue.data(false);
    // await Future.delayed(delay ?? ActionDurations.afterClickDuration);
    ref.refresh(userLoggedInProvider);
  }
}
