
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

final fetchPolicyProvider = StateProvider<FetchPolicy>((ref) => FetchPolicy.cacheFirst);

// void resetFetchPolicy() {
//
// }