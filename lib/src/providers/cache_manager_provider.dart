import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final cacheManagerProvider = Provider<BaseCacheManager>((ref) {
  return CacheManager(
    Config(
      'jimiAppCachedData',
      stalePeriod: const Duration(days: 365),
      maxNrOfCacheObjects: 9999999,
    ),
  );
});
