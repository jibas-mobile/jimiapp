import 'dart:convert';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../features/login/domain/organisasi.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

final secureStorageServiceProvider =
    Provider<SecureStorageService>((ref) => throw UnimplementedError());

class SecureStorageService {
  SecureStorageService(this.flutterSecureStorage);
  final FlutterSecureStorage flutterSecureStorage;

  static const organisasiKey = 'organisasi';
  static const userAuthKey = 'userAuth';

  final iosOptions = const IOSOptions();
  final androidOptions = const AndroidOptions(encryptedSharedPreferences: true);

  Future<void> setOrganisasi(Organisasi organisasi) async {
    await flutterSecureStorage.write(
        key: organisasiKey,
        value: jsonEncode(organisasi.toJson()),
        iOptions: iosOptions,
        aOptions: androidOptions);
  }

  Future<Organisasi?> getOrganisasi() async {
    final String? json = await flutterSecureStorage.read(
        key: organisasiKey, iOptions: iosOptions, aOptions: androidOptions);
    if (json == null) {
      return null;
    }
    return Organisasi.fromJson(jsonDecode(json));
  }

  Future<void> clearOrganisasi() async {
    await flutterSecureStorage.delete(
        key: organisasiKey, iOptions: iosOptions, aOptions: androidOptions);
  }

  Future<void> setAuthKey(String key) async {
    await flutterSecureStorage.write(
        key: userAuthKey,
        value: key,
        iOptions: iosOptions,
        aOptions: androidOptions);
  }

  Future<String?> getAuthKey() async {
    return await flutterSecureStorage.read(
        key: userAuthKey, iOptions: iosOptions, aOptions: androidOptions);
  }

  Future<void> clearAuthKey() async {
    await flutterSecureStorage.delete(
        key: userAuthKey, iOptions: iosOptions, aOptions: androidOptions);
  }
}
