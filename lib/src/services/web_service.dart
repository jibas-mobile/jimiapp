import 'dart:io';

import 'package:dio/dio.dart';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../utils/top_providers.dart';
import '../features/login/domain/organisasi.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:logger/logger.dart';
import 'package:validators/sanitizers.dart';

class LoginException implements Exception {
  final String _message;
  LoginException(this._message);

  @override
  String toString() {
    return _message;
  }
}

final dioProvider = Provider<Dio>((ref) => Dio());

final authRepositoryProvider = Provider<AuthWebRepository>((ref) {
  final dio = ref.watch(dioProvider);
  final logger = ref.watch(loggerProvider);
  return AuthWebRepository(dio, logger);
});

class AuthWebRepository {
  AuthWebRepository(this.dio, this.logger) {
    final orgUrl = dotenv.maybeGet('ORG_LIST_URL');
    assert(orgUrl != null);
    organisasiUrl = rtrim(orgUrl!, '/');
  }

  final Dio dio;
  final Logger logger;
  late String organisasiUrl;

  Future<Organisasi?> getOrganisasiByKode(String kode) async {
    try {
      final url = '$organisasiUrl/$kode';
      final response = await dio.get(url);
      return Organisasi.fromJson(response.data);
    } on DioError catch (e) {
      if (e.response?.statusCode == 404) {
        return null;
      }
      rethrow;
    }
  }

  Future<String> loginPegawai(String jimiUrl,
      username, password) async {

    try {
      final url = rtrim(jimiUrl, '/') + '/api/auth/pegawai';
      logger.d('Logging in to $url');
      final response = await dio.post(url, data: {
        'username': username,
        'password': password
      });
      if (response.statusCode != 201) {
        logger.d('Invalid response status code = ${response.statusCode}');
        throw LoginException('invalid response from server');
      }
      return response.data['key'];
    } on SocketException catch (e) {
      throw LoginException('server is unavailable at the moment, please retry later');
    } on DioError catch (e) {
      logger.d('Response status='+ (e.response?.statusCode?.toString() ?? 'null'));
      switch (e.response?.statusCode) {
        case 404:
          throw LoginException('unable to login right now');
        case 401:
          throw LoginException('invalid username or password');
        case 403:
          throw LoginException('user is inactive');
        case 500:
          throw LoginException('server is busy');
      }
    } catch (e) {
      throw LoginException('invalid response from server: ${e.toString()}');
    }
    throw LoginException('invalid response from server');
  }
}
