
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../features/login/domain/organisasi.dart';

import 'secure_storage_service.dart';
import 'web_service.dart';

final currentOrganisasiProvider = FutureProvider<Organisasi?>((ref) => ref.watch(secureStorageServiceProvider).getOrganisasi());

final organisasiServiceProvider = StateNotifierProvider<OrganisasiService, AsyncValue<Organisasi?>>((ref) {
  final webService = ref.watch(authRepositoryProvider);
  final sharedPreferencesService = ref.watch(secureStorageServiceProvider);
  final organisasi = ref.watch(currentOrganisasiProvider);
  return OrganisasiService(webService: webService,
      secureStorageService: sharedPreferencesService,
      currentOrganisasi: organisasi);
});


class OrganisasiService extends StateNotifier<AsyncValue<Organisasi?>> {
  OrganisasiService({required this.webService, required this.secureStorageService,
    required AsyncValue<Organisasi?> currentOrganisasi}):
      super(currentOrganisasi);

  final AuthWebRepository webService;
  final SecureStorageService secureStorageService;

  Future<void> selectOrganisasi(String kode) async {
    if (kode.length < 4) {
      state = AsyncValue.error(tr('organisation_form.min_len_msg'));
      return;
    }
    try {
      state = const AsyncValue.loading();
      final org = await webService.getOrganisasiByKode(kode);
      if (org != null) {
        await secureStorageService.setOrganisasi(org);
        state = AsyncValue.data(org);
      } else {
        state = AsyncValue.error(tr('organisation_service.org_not_found')+kode);
      }
    } catch (e) {
      state = AsyncValue.error(e.toString());
    }
  }

  Future<void> clearOrganisasi() async {
    await secureStorageService.clearOrganisasi();
    state = const AsyncValue.data(null);
  }

  Organisasi? get getOrganisasi {
    try {
      return state.value;
    } catch (e) {
      return null;
    }
  }
}