import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:jimiapp/src/features/login/domain/jimi_user.dart';

import '../features/login/application/auth_service.dart';
import 'graphql_queries.dart';
import 'organisasi_service.dart';

final graphqlProvider = FutureProvider<GraphQLService>((ref) async {
  final gqlClient = await ref.watch(graphQLClientProvider.future);
  return GraphQLService(gqlClient);
});

final getMeInfoProvider = FutureProvider<JimiUser>((ref) async {
  final gqlService = await ref.watch(graphqlProvider.future);
  return await gqlService.me();
});

final graphQLClientProvider = FutureProvider<GraphQLClient>((ref) async {
  final organisasi = await ref.watch(currentOrganisasiProvider.future);
  final authKey = await ref.watch(getAuthKeyProvider.future);

  final httpLink = HttpLink(organisasi!.apiUrl + '/graphql');
  final authLink = AuthLink(
    getToken: () async => 'Bearer $authKey',
  );
  final Link link = authLink.concat(httpLink);
  debugPrint('GraphQL initialised');
  return GraphQLClient(link: link, cache: GraphQLCache());
});

// TODO this is a repostory
class GraphQLService {
  GraphQLService(this.client);

  final GraphQLClient client;

  // get this out of here
  Future<JimiUser> me() async {
    debugPrint('GraphQL me me me');
    final options = QueryOptions(
      document: gql(Queries.me),
      errorPolicy: ErrorPolicy.all,
      cacheRereadPolicy: CacheRereadPolicy.ignoreAll,
    );
    try {
      final result = await client.query(options);
      if (result.exception?.linkException is ServerException) {
        throw result.exception!.linkException!;
      }
      final jimiUser = JimiUser.fromJson(result.data!['me']);
      return jimiUser;
    } catch (e) {
      rethrow;
    }
  }
}
