abstract class Queries {
  static const String me = r'''
    query {
      me {
        id
        jibasLogin
        type
        pegawai: jibasUser {
          __typename
          ... on Pegawai {
            id
            nama
            nip
            bagian {
              id
              nama
            }
            guru {
              id
              pelajaran {
                id
                nama
                departemen {
                  id
                  nama
                }
              }
            }
          }
        }
      }
    }
  ''';

  static const String waliKelasPegawaiByID = r'''
  query 
    getKelasByPegawai($id: ID!) {
        pegawai(id: $id) {
        kelasSet {
          pageInfo {
            hasNextPage
          }
          edges {
            node {
              id
              nama
              tingkat {
                id
                nama
                departemen {
                  id
                  nama
                }
              }
            }
          }			
        }
      }
    }
  ''';
}
