
class ActionDurations {
  static Duration get afterClickDuration => const Duration(seconds: 3);
  static Duration get wholeYearDuration => const Duration(days: 365);
}