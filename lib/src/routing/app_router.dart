import 'package:flutter/material.dart';
import '../features/akademik/presentation/kelas/kelas_detail_page.dart';

import '../features/akademik/domain/kelas.dart';
import '../features/akademik/domain/siswa.dart';
import '../features/akademik/presentation/siswa/siswa_detail_page.dart';
import '../features/search/presentation/search_form.dart';

class AppRoutes {
  static const searchPage = '/search-page';
  static const viewSiswaPage = '/view-siswa-page';
  static const takePicturePage = '/take-picture';
  static const viewKelasPage = '/view-kelas-page';
}

class AppRouter {
  static Route<dynamic>? onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case AppRoutes.searchPage:
        return MaterialPageRoute<dynamic>(
          builder: (_) => const SearchForm(),
          settings: settings,
          fullscreenDialog: false,
        );
      case AppRoutes.viewSiswaPage:
        return MaterialPageRoute(
          builder: (_) => SiswaDetailPage(siswa: args as Siswa),
          settings: settings,
          fullscreenDialog: false,
        );
      case AppRoutes.viewKelasPage:
        return MaterialPageRoute(
          builder: (_) => KelasDetailPage(kelas: args as Kelas),
          settings: settings,
        );
      // case AppRoutes.takePicturePage:
      //   final mapArgs = args as Map<String, dynamic>;
      //   return MaterialPageRoute(
      //     builder: (_) => TakePictureScreen(),
      //     settings: settings,
      //     fullscreenDialog: false,
      //   );
    }
  }
}
