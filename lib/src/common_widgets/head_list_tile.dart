import 'package:flutter/material.dart';

class HeadListTile extends StatelessWidget {
  const HeadListTile(
      this.title, {
        Key? key,
      }) : super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(title, style: Theme.of(context).textTheme.headline5),
      dense: true,
    );
  }
}