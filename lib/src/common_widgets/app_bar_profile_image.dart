import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:jimiapp/src/providers/cache_manager_provider.dart';

import '../features/login/application/auth_service.dart';
import '../utils/top_providers.dart';

final refreshAppBarProfileImageProvider =
    Provider.autoDispose.family<void, String>((ref, url) {
  // none, nada
});

class AppBarProfileImage extends ConsumerWidget {
  const AppBarProfileImage({
    Key? key,
    required this.url,
    this.cacheKey,
  }) : super(key: key);

  final String url;
  final String? cacheKey;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final logger = ref.read(loggerProvider);
    final refresh = ref.watch(refreshAppBarProfileImageProvider(url));
    final authKey = ref.watch(getAuthKeyProvider).value!;
    final cacheManager = ref.watch(cacheManagerProvider);
    logger.d('refresh appbarprofileimage: $url');
    return CachedNetworkImage(
      cacheManager: cacheManager,
      httpHeaders: {'Authorization': 'Bearer ${authKey}'},
      placeholder: (context, url) => const CircularProgressIndicator(),
      imageUrl: url,
      fit: BoxFit.contain,
      errorWidget: (_, __, err) {
        // TODO snackbar on err
        return Center(
            child: FaIcon(
          FontAwesomeIcons.userLargeSlash,
          size: 72.0,
          color: Theme.of(context).appBarTheme.iconTheme!.color,
        ));
      },
    );
  }
}
