import 'package:flutter/material.dart';

class TwoColumn extends StatelessWidget {
  const TwoColumn(
      this.row1,
      this.row2, {
        Key? key,
      }) : super(key: key);

  final Widget row1, row2;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          row1,
          row2,
        ],
      ),
    );
  }
}