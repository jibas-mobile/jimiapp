
import 'package:flutter/material.dart';

class SeparasiBaris extends StatelessWidget {
  const SeparasiBaris({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(height: 16.0);
  }
}
