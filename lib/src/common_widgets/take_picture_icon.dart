import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class TakePictureIcon extends StatelessWidget {
  const TakePictureIcon({
    Key? key,
    required this.onSelected,
  }) : super(key: key);

  final void Function(XFile? file)? onSelected;

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<ImageSource>(
      icon: const Icon(Icons.photo_camera),
      onSelected: (selected) async {
        final ImagePicker _picker = ImagePicker();
        final XFile? image = await _picker.pickImage(
          source: selected,
          maxWidth: 1920,
          maxHeight: 1280,
          imageQuality: 50,
        );
        onSelected?.call(image);
      },
      itemBuilder: (context) => <PopupMenuEntry<ImageSource>>[
        PopupMenuItem<ImageSource>(
          value: ImageSource.camera,
          child: Text('common.use_camera'.tr()),
        ),
        PopupMenuItem<ImageSource>(
          value: ImageSource.gallery,
          child: Text('common.use_gallery'.tr()),
        ),
      ],
    );
  }
}