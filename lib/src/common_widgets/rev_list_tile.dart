import 'package:flutter/material.dart';

class RevListTile extends StatelessWidget {
  const RevListTile({
    Key? key,
    required this.title,
    required this.value,
  }) : super(key: key);

  final String title;
  final String? value;

  @override
  Widget build(BuildContext context) {
    String content = value ?? '';
    return ListTile(
      dense: true,
      title: Text(title, style: Theme.of(context).textTheme.caption),
      subtitle: Text(content == '' ? '-' : content, style: Theme.of(context).textTheme.bodyText1,),
    );
  }
}
