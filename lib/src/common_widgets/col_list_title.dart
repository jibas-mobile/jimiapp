import 'package:flutter/material.dart';

class ColListTile extends StatelessWidget {
  const ColListTile({
    Key? key,
    required this.title,
    required this.value,
  }) : super(key: key);

  final String title;
  final String? value;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title, style: Theme.of(context).textTheme.caption),
          Text(value ?? '-', style: Theme.of(context).textTheme.bodyText1),
        ],
      ),
    );
  }
}