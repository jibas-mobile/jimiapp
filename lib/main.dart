import 'dart:io';
import 'dart:typed_data';

import 'package:easy_localization/easy_localization.dart';
import 'package:flex_color_scheme/flex_color_scheme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'src/features/home/presentation/home_page.dart';
import 'src/features/login/domain/organisasi.dart';
import 'src/features/login/presentation/auth_widget.dart';
import 'src/features/login/presentation/login_form.dart';
import 'src/features/login/presentation/organisation_form.dart';
import 'src/routing/app_router.dart';
import 'src/services/organisasi_service.dart';
import 'src/services/secure_storage_service.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  await dotenv.load(fileName: ".env");
  const secureStorage = FlutterSecureStorage();

  ByteData data = await PlatformAssetBundle().load('assets/ca/mockable-io.pem');
  SecurityContext.defaultContext
      .setTrustedCertificatesBytes(data.buffer.asUint8List());

  runApp(ProviderScope(
    overrides: [
      secureStorageServiceProvider.overrideWithValue(
        SecureStorageService(secureStorage),
      ),
    ],
    child: EasyLocalization(
      supportedLocales: const [Locale('en'), Locale('id')],
      path: 'assets/translations',
      fallbackLocale: const Locale('en'),
      child: const MyApp(),
    ),
  ));
}

class MyApp extends ConsumerWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    Organisasi? organisasi;
    try {
      final futureOrg = ref.watch(currentOrganisasiProvider);
      organisasi = futureOrg.value;
    } catch (e) {
      organisasi = null;
    }

    return MaterialApp(
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      title: 'JIMI',
      debugShowCheckedModeBanner: false,
      theme: FlexThemeData.light(scheme: FlexScheme.aquaBlue),
      darkTheme: FlexThemeData.dark(scheme: FlexScheme.aquaBlue),
      themeMode: ThemeMode.light, // TODO fix dark mode
      home: AuthWidget(
        nonSignedInBuilder: (_) =>
            organisasi == null ? const OrganisationForm() : const LoginForm(),
        signedInBuilder: (_) => const HomePage(),
      ),
      onGenerateRoute: (settings) => AppRouter.onGenerateRoute(settings),
    );
  }
}
