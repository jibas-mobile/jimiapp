// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:jimiapp/main.dart';
import 'package:jimiapp/src/services/secure_storage_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  TestWidgetsFlutterBinding.ensureInitialized();

  Map<String, Object> values = <String, Object>{};
  SharedPreferences.setMockInitialValues(values);

  await EasyLocalization.ensureInitialized();


  testWidgets('Homepage should ask for org key', (WidgetTester tester) async {
    // Empty shared preferences

    final secureStorage = FlutterSecureStorage();

    // Build our app and trigger a frame.
    await tester.pumpWidget(ProviderScope(
      overrides: [
        secureStorageServiceProvider.overrideWithValue(SecureStorageService(secureStorage)),
      ],
      child: EasyLocalization(
        supportedLocales: [Locale('en'), Locale('id')],
        path: 'assets/translations',
        fallbackLocale: Locale('en'),
        child: MyApp(),
      )
    ));

    await tester.pump();

    // Verify that our counter starts at 0.
    expect(find.byKey(ValueKey("inputOrgKey")), findsOneWidget);

  });
}
