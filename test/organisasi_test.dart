import 'dart:io';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:dio/dio.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';
import 'package:jimiapp/src/services/web_service.dart';
import 'package:logger/logger.dart';


void main() async {
  group('organisasi', () {
    setUp(() {
      print(Directory.current.toString());
      dotenv.testLoad(fileInput: 'ORG_LIST_URL=https://demo2278480.mockable.io/organisation/'); // mergeWith: Platform.environment
    });

    test('get organisasi gagal maning', () async {
      final dio = Dio();
      final dioAdapter = DioAdapter(dio: dio);
      final logger = Logger();

      dioAdapter.onGet(
        'https://demo2278480.mockable.io/organisation/smasatu',
            (server) => server.reply(404, {
          'error':'not found'
        }),
      );

      final container = ProviderContainer(
          overrides: [
            authRepositoryProvider.overrideWithValue(AuthWebRepository(dio, logger))
          ]
      );
      addTearDown(container.dispose);

      final webService = await container.read(authRepositoryProvider);

      final organisasi = await webService.getOrganisasiByKode('smasatu');

      expect(null, organisasi);
    });

    test('get organisasi sukses', () async {
      final dio = Dio();
      final dioAdapter = DioAdapter(dio: dio);
      final logger = Logger();

      dioAdapter.onGet(
        'https://demo2278480.mockable.io/organisation/smasatu',
            (server) => server.reply(200, {
          'nama':'SMA Bleketuk',
          'aktif': true,
          'apiUrl': 'http://localhost:8000/'
        }),
      );

      final container = ProviderContainer(
          overrides: [
            authRepositoryProvider.overrideWithValue(AuthWebRepository(dio, logger))
          ]
      );
      addTearDown(container.dispose);

      final webService = await container.read(authRepositoryProvider);
      final organisasi = await webService.getOrganisasiByKode('smasatu');

      expect(organisasi?.nama, 'SMA Bleketuk');
      expect(organisasi?.aktif, true);
      expect(organisasi?.apiUrl, 'http://localhost:8000/');
    });
  });

}
